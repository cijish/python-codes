sudo apt-get update && apt-get -y upgrade
sudo apt-get install git wkhtmltopdf python-pip python-dev \
    python-virtualenv libevent-dev gcc libjpeg-dev libxml2-dev \
    libssl-dev libsasl2-dev node-less libldap2-dev libxslt-dev
sudo apt-get install postgresql-9.5 postgresql-server-dev-9.5
systemctl enable postgresql.service
systemctl start postgresql.service
